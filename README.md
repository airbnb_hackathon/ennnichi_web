ennnichi
====

Overview

## 概要

## デモ

## VS. 

## Requirement

- Docker
- Docker Compose

## デプロイ方法

#### ローカルでの実行

##### makeコマンドが利用できる場合

実行
```sh
make run
```
ストップ
```sh
make stop
```

##### makeコマンドが利用できず、Dockerコマンド、Docker-Composeコマンドが利用できる場合

実行
```sh
docker-compose build
docker-compose up -d
```

ストップ
```sh
docker stop ennnichi_web_nginx_1 ennnichi_web_uwsgi_1
docker rm ennnichi_web_nginx_1 ennnichi_web_uwsgi_1
```

##### makeコマンド、Dockerコマンド、Docker-Composeコマンドが利用できない場合

前提条件
```sh
pip install boto3 Flask
```

実行（Nginx、uwsgiの利用なし。）
```sh
python ./app/src/main.py 
```

ストップ
Ctrl-C

#### サーバー上での実行

##### ennnichi-userでのsshログイン

```sh
ssh ennnichi-user@52.193.142.131
```
```
ennnichi-user@52.193.142.131's password:P@ssw0rd
```

##### アプリのディレクトリに移動

```sh
cd ennnichi_web
```

##### アプリを最新化

```sh
git pull 
```

もし聞かれたら、、、
```sh
Password for 'https://hikaruhasegawa@bitbucket.org':P@ssw0rd1
```

##### アプリのデプロイ

```sh
make run
```

##### アプリへのアクセス

http://52.193.142.131

##### アプリの終了

```sh
make stop
```