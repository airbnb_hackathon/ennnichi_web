run:
	docker-compose build
	docker-compose up -d

stop:
	docker stop ennnichiweb_nginx_1 ennnichiweb_uwsgi_1
	docker rm ennnichiweb_nginx_1 ennnichiweb_uwsgi_1
