from flask import Flask, render_template, request,redirect, url_for
import json
import boto3
import requests
app = Flask(__name__)
app.debug = True #debugモードにすることで、app.loggerが標準出力になる

# AWS S3の設定
bucket_name = 'ennnichi-bucket'
s3 = boto3.resource('s3')
@app.route('/')
def index():
    return redirect(url_for('regist'))

@app.route('/cond', methods=['GET', 'POST'])
def cond():
    if request.method == 'POST':
        app.logger.debug('condition: POST')

        #ユーザ情報を送信
        payload = {'name': request.form.get('name'), 'hobby1': request.form.get('hobby1'), 'hobby2': request.form.get('hobby2'), 'skill1': request.form.get('skill1'), 'skill2': request.form.get('skill2')}
        regist_post_url = 'https://o9enooe0nh.execute-api.ap-northeast-1.amazonaws.com/default/ennnichi-cond-func'

        requests.post(regist_post_url, data=payload)
        app.logger.debug('condition: search user details')

        return render_template('condition.html')
    else:
        app.logger.debug('condition: GET')
        return render_template('condition.html')

@app.route('/regist', methods=['GET', 'POST'])
def regist():
    if request.method == 'POST':
        app.logger.debug('regist: POST')

        #postで送られた画像を取得
        image = request.files['img']
        app.logger.debug('regist:' + image)

        if '' == image.filename:
            return render_template('regist.html')

        #サーバーのローカルストレージに画像を一旦保存
        image.save('local.jpg')
        app.logger.debug('regist: image saved')

        #ローカルに保存した画像をS3にアップロード
        s3.Bucket(bucket_name).upload_file('local.jpg', request.form.get('userName') + '.jpg')
        app.logger.debug('regist: image uploaded')

        #ユーザ情報を登録
        payload = {'userName': request.form.get('userName'), 'name': request.form.get('name'), 'hobby1': request.form.get('hobby1'), 'hobby2': request.form.get('hobby2'), 'skill1': request.form.get('skill1'), 'skill2': request.form.get('skill2')}
        regist_post_url = 'https://rsn9nfs08d.execute-api.ap-northeast-1.amazonaws.com/default/ennnichi_register_func'

        requests.post(regist_post_url, data=payload)
        app.logger.debug('regist: saved user detail')

        #とりあえず元の画面に戻っておく
        return render_template('regist.html')
    else:
        app.logger.debug('GET')
        return render_template('regist.html')

@app.route('/matching')
def matching():
   return render_template('matching.html')


if __name__ == '__main__':
    app.run()